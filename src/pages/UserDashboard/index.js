import React, { Fragment, useEffect } from "react";
import faker from "faker";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";
import { NavLink } from "react-router-dom";
import ProjectCard from "../../../components/ProjectCard";
import NewsCard from "../../../components/NewsCard";
import Loader from "../../../components/Loader";

import routes from "../../constants/routes";

import LocationCityIcon from "@material-ui/icons/LocationCity";
import ChromeReaderModeIcon from "@material-ui/icons/ChromeReaderMode";

import style from "./style.module.scss";

const news = [
  {
    id: faker.random.number(),
    createdAt: faker.date.past(),
    title: faker.lorem.sentence(),
    image: "https://source.unsplash.com/random?building",
  },
  {
    id: faker.random.number(),
    createdAt: faker.date.past(),
    title: faker.lorem.sentence(),
    image: "https://source.unsplash.com/random?city",
  },
  {
    id: faker.random.number(),
    createdAt: faker.date.past(),
    title: faker.lorem.sentence(),
    image: "https://source.unsplash.com/random?philipine",
  },
  {
    id: faker.random.number(),
    createdAt: faker.date.past(),
    title: faker.lorem.sentence(),
    image: "https://source.unsplash.com/random?construction",
  },
];

const UserDashboard = ({ readProjects, projectData }) => {
  useEffect(() => {
    readProjects();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Fragment>
      {projectData.length === 0 ? (
        <Loader />
      ) : (
        <Fragment>
          <Grid
            container
            direction="row"
            alignItems="center"
            justify="space-between"
          >
            <Typography variant="subtitle2" gutterBottom>
              <LocationCityIcon className={style.title__icon} />
              Available Projects
            </Typography>
            <Typography display="inline" variant="caption" gutterBottom>
              <Link
                color="inherit"
                component={NavLink}
                to={routes.dashboard.projects.overview}
              >
                {`View All >`}
              </Link>
            </Typography>
          </Grid>
          <Grid container spacing={3}>
            <ProjectCard
              key={"poof"}
              project={{
                id: "1",
                project_name:
                  "31-Storey Condominium with basement and roof deck",
                project_address: "8575 Nowlin Rd, Alabama 143 United States",
                description:
                  "Eu id et pariatur aliqua pariatur tempor non dolor sint. Laborum minim in sint adipisicing cupidatat id pariatur velit consectetur cupidatat adipisicing sint. Eu ad ullamco fugiat deserunt elit dolor labore in mollit non aliquip ut nostrud nostrud. Ad pariatur pariatur veniam adipisicing enim. Dolor nostrud enim aliquip reprehenderit amet cupidatat nisi est aliqua pariatur.",
                type: "Condo",
                project_pictures: [],
              }}
            />
            {projectData.map((value) => {
              return <ProjectCard key={value.name} project={value} />;
            })}
          </Grid>
        </Fragment>
      )}

      <div className="spacer__3" />
      <Grid
        container
        direction="row"
        alignItems="center"
        justify="space-between"
      >
        <Typography display="inline" variant="subtitle2" gutterBottom>
          <ChromeReaderModeIcon className={style.title__icon} />
          Latest News
        </Typography>
        <Typography display="inline" variant="caption" gutterBottom>
          {`View All >`}
        </Typography>

        <Grid container spacing={2}>
          {news.map((value, index) => {
            return <NewsCard key={`news-card-${index}`} news={value} />;
          })}
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default UserDashboard;
