import React, { Fragment, useState } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { Formik } from "formik";
import * as Yup from "yup";

import routes from "../../constants/routes";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";

import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";

import style from "./style.module.scss";

const Login = ({ loginHandler, userData }) => {
  const [showPassword, setShowPassword] = useState(false);

  const handleLoginAttempt = (values) => {
    loginHandler(values);
  };

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  return (
    <Fragment>
      <Grid className={style["container__grid"]} container>
        <Grid
          container
          item
          xs={12}
          sm={8}
          md={6}
          alignItems="center"
          justify="flex-end"
        >
          <Grid
            item
            container
            direction="column"
            justify="center"
            xs={12}
            sm={8}
            md={8}
            className={style["login-card"]}
          >
            <Grid
              item
              container
              direction="row"
              justify="flex-start"
              alignItems="center"
              xs={12}
            >
              <Grid item>
                <Typography
                  variant="h5"
                  align="left"
                  className={style["login-card__signin-items"]}
                >
                  Sign In
                </Typography>
              </Grid>
              <Grid item>
                <Typography variant="caption">
                  Dont have an Account?{" "}
                  <Link to={routes.dashboard.home} className="link--invisible">
                    <Typography variant="caption" color="error">
                      {" "}
                      Create an Account{" "}
                    </Typography>
                  </Link>
                </Typography>
              </Grid>
            </Grid>
            <div className="spacer__1" />
            <Formik
              initialValues={{ email: "person@gmail.com", password: "" }}
              onSubmit={(values, { setSubmitting }) => {
                setSubmitting(true);
                handleLoginAttempt(values);
              }}
              validationSchema={Yup.object().shape({
                email: Yup.string().email().required("Email is required"),
                password: Yup.string().required("Password is required"),
              })}
            >
              {(props) => {
                const {
                  values,
                  touched,
                  errors,
                  dirty,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                } = props;
                return (
                  <form onSubmit={handleSubmit}>
                    <Grid
                      container
                      item
                      direction="column"
                      alignItems="stretch"
                      spacing={2}
                      xs={12}
                    >
                      <Grid item xs={12}>
                        <TextField
                          label="Email"
                          placeholder="Email Address"
                          name="email"
                          type="text"
                          fullWidth
                          value={values.email}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          error={errors.email && touched.email}
                          helperText={
                            errors.email && touched.email && errors.email
                          }
                          margin="normal"
                          InputLabelProps={{
                            shrink: true,
                          }}
                        />
                      </Grid>

                      <Grid item xs={12}>
                        <TextField
                          label="Password"
                          placeholder="Password"
                          name="password"
                          type={showPassword ? "text" : "password"}
                          fullWidth
                          value={values.password}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          error={errors.password && touched.password}
                          helperText={
                            errors.password &&
                            touched.password &&
                            errors.password
                          }
                          margin="normal"
                          InputProps={{
                            endAdornment: (
                              <InputAdornment position="start">
                                <IconButton
                                  aria-label="toggle password visibility"
                                  onClick={handleClickShowPassword}
                                  onMouseDown={handleMouseDownPassword}
                                  edge="end"
                                >
                                  {showPassword ? (
                                    <Visibility />
                                  ) : (
                                    <VisibilityOff />
                                  )}
                                </IconButton>
                              </InputAdornment>
                            ),
                          }}
                          InputLabelProps={{
                            shrink: true,
                          }}
                        />
                        <Grid item container justify="flex-end">
                          <Link
                            to={routes.dashboard.home}
                            className="link--invisible"
                          >
                            <Typography
                              variant="caption"
                              color="secondary"
                              align="right"
                            >
                              Forgot Password
                            </Typography>
                          </Link>
                        </Grid>
                      </Grid>

                      <div className="spacer__1" />

                      <Grid
                        container
                        item
                        xs={12}
                        direction="row"
                        justify="flex-start"
                      >
                        <Button
                          type="submit"
                          color="primary"
                          variant="contained"
                          size="large"
                          disabled={!dirty}
                          disableElevation
                        >
                          Sign In
                        </Button>
                      </Grid>
                    </Grid>
                  </form>
                );
              }}
            </Formik>
          </Grid>
        </Grid>
        <Grid
          container
          item
          xs={12}
          sm={8}
          md={6}
          alignItems="center"
          justify="center"
        >
          <div>
            {userData && (
              <Fragment>
                <Typography variant="h6">{`${userData.name.title} ${userData.name.first} ${userData.name.last}`}</Typography>
                <Typography variant="body1">{userData.email}</Typography>
                <Typography variant="body1">{userData.gender}</Typography>
                <Typography variant="body1">{userData.registered.age}</Typography>
              </Fragment>
            )}
          </div>
        </Grid>
      </Grid>
    </Fragment>
  );
};

Login.propTypes = {
  loginHandler: PropTypes.func.isRequired,
};

Login.defaultProps = {
  userData: [],
};

export default Login;
