import React from "react";
import { useRouteMatch } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import routes from "../../constants/routes";

import Login from "../../pages/Login";

import { doLogin } from "../../redux/actionCreators/authentication";
import { selectUserData } from "../../redux/selectors/authentication";

const AuthenticationContainer = ({ loginHandler, userData }) => {
  const location = useRouteMatch().path;
  switch (location) {
    case routes.public.login:
      return <Login loginHandler={loginHandler} userData={userData} />;
    default:
      return false;
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    loginHandler: (payload) => dispatch(doLogin(payload)),
  };
};

const mapStateToProps = (state, ownProps) => {
  return {
    userData: selectUserData(state),
  };
};

AuthenticationContainer.propTypes = {
  loginHandler: PropTypes.func.isRequired,
  userData: PropTypes.object,
};

AuthenticationContainer.defaultProps = {
  userData: {},
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthenticationContainer);
