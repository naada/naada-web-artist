import { createMuiTheme } from "@material-ui/core/styles";
import grey from "@material-ui/core/colors/grey";

const theme = {
  palette: {
    type: "light",
    primary: {
      light: "#ef5350",
      main: "#f44336",
      dark: "#d32f2f",
      contrastText: "#fff",
    },
    secondary: {
      light: "#424242",
      main: "#212121",
      dark: "#000000",
      contrastText: "#fff",
    },
  },
  overrides: {
    MuiAppBar: {
      colorPrimary: {
        backgroundColor: grey[100],
        // boxShadow: 'none',
      },
    },
    MuiDrawer: {
      paper: {
        backgroundColor: grey[100],
      },
    },
  },
};

const muiTheme = createMuiTheme(theme);

export default muiTheme;