import { include } from "named-urls";

export default {
  public: include("/", {
    login: "login",
  }),
  dashboard: include("/", {
    home: "dashboard",
  }),
};
