import axios from "axios";
// import faker from "faker";

// const url = process.env.REACT_APP_BACKEND;

const headers = {
  "Content-Type": "application/json",
};

const axiosInstance = axios.create({
  // baseURL: `${url}api/`,
  baseURL: ``,
  headers: headers,
});

axiosInstance.interceptors.request.use(
  function (config) {
    let token = localStorage.getItem("token");
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

// Authentication services
export const Login = (payload) => {
  let userData = {
    email: payload.email,
    password: payload.password,
  };

  const url = "https://randomuser.me/api/";
  return axiosInstance({
    method: "GET",
    url: url,
    data: userData,
  });
};
