import { handleActions } from "redux-actions";

import * as actionCreators from "../actionCreators/authentication";

const defaultState = {
  isLoading: false,
  userData: null,
};

const authenticationReducer = handleActions(
  {
    [actionCreators.doLogin]: (state) => {
      return { ...state, isLoading: true };
    },
    [actionCreators.doLoginSuccess]: (
      state,
      {
        payload: {
          data: { results = {} },
        },
      }
    ) => {
      return {
        ...state,
        userData: results[0],
      };
    },
    [actionCreators.doLoginFailure]: (state) => {
      return { ...state, isLoading: false };
    },
  },
  defaultState
);

export default authenticationReducer;
