export const selectIsVisible = state => state.miscellaneous.isVisible;
export const selectNotificationMessage = state => state.miscellaneous.message;
export const selectIsLoadingVisible = state => state.miscellaneous.isLoadingVisible;
export const selectRedirectTo = state => state.miscellaneous.redirectTo;