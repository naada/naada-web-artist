import { call, put, takeLatest } from "redux-saga/effects";

import * as actionCreators from "../actionCreators/authentication";
import * as actionTypes from "../actionTypes/authentication";

import * as misc from "../actionCreators/miscellaneous";

import * as APIService from "../../services/APIService";

function* requestLogin({ payload }) {
  // yield put(misc.doToggleLoadingBar({ isLoadingVisible: true }));
  try {
    const response = yield call(APIService.Login, payload);
    yield put(actionCreators.doLoginSuccess(response));
    const message = "Successfully Fetched User";
    yield put(misc.doCreateNotification({ isVisible: true, message }));
  } catch (error) {
    const message = error.response.data.message || "default message";
    yield put(misc.doCreateNotification({ isVisible: true, message }));
    yield put(actionCreators.doLoginFailure(error));
  } finally {
    // yield put(misc.doToggleLoadingBar({ isLoadingVisible: false }));
  }
}

const authenticationSaga = [
  takeLatest(actionTypes.AUTH_LOGIN_REQUESTED, requestLogin),
];

export default authenticationSaga;
