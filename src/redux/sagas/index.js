import { all } from "redux-saga/effects";

import authenticationSaga from "./authenticationSaga";

function* watchAll() {
  yield all([...authenticationSaga]);
}

export default watchAll;
