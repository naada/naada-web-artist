import { createAction } from "redux-actions";

import * as actionTypes from "../actionTypes/miscellaneous";

export const doCreateNotification = createAction(actionTypes.NOTIFICATION_CREATE_REQUEST);
