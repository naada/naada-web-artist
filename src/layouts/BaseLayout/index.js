import React, { Fragment } from "react";
import { Route } from "react-router-dom";

import NotificationContainer from "../../containers/NotificationContainer";

const BaseLayout = ({ component: Component, ...rest }) => (
  <Route
    render={(props) => (
      <Fragment>
        <div>
          <Component {...props} />
        </div>
        <NotificationContainer />
      </Fragment>
    )}
    {...rest}
  />
);

export default BaseLayout;
