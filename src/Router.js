import React from "react";
import { Switch } from "react-router-dom";
import routes from "./constants/routes";
import AuthenticationContainer from "./containers/AuthenticationContainer";
import BaseLayout from "./layouts/BaseLayout";

const Router = () => {
  return (
    <Switch>
      <BaseLayout
        exact
        path={routes.public.login}
        component={AuthenticationContainer}
      />
      <BaseLayout
        exact
        path={routes.dashboard.home}
        component={AuthenticationContainer}
      />
    </Switch>
  );
};

export default Router;
